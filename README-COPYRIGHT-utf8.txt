Copyright CNRS/Inria/UCA
Contributor(s): Eric Debreuve (since 2021)

eric.debreuve@cnrs.fr

This software is being developed by Eric Debreuve, a CNRS employee and member of team Morpheme.
Team Morpheme is a joint team between Inria, CNRS, and UCA.
It is hosted by the Centre Inria d'Université Côte d'Azur, Laboratory I3S, and Laboratory iBV.

CNRS: https://www.cnrs.fr/index.php/en
Inria: https://www.inria.fr/en/
UCA: https://univ-cotedazur.eu/
Centre Inria d'Université Côte d'Azur: https://www.inria.fr/en/centre/sophia/
I3S: https://www.i3s.unice.fr/en/
iBV: http://ibv.unice.fr/
Team Morpheme: https://team.inria.fr/morpheme/
