# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2021)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from typing import Annotated

from conf_ini_g.specification.config import config_t
from conf_ini_g.specification.constraint import (
    boolean_t,
    choices_t,
    number_t,
    path_t,
    sequence_t,
)
from conf_ini_g.specification.default import missing_required_value_t
from conf_ini_g.specification.parameter import parameter_t
from conf_ini_g.specification.section import controller_t, section_t
from conf_ini_g.standard.path_extension import path_t as pl_path_t


_SECTIONS = (
    section_t(
        name="Section_1",
        parameters=[
            parameter_t(
                name="Parameter_1",
                default=missing_required_value_t(
                    (
                        Annotated[int, number_t(min=10)],
                        Annotated[bool, boolean_t(mode=boolean_t.MODE.yes_no)],
                    )
                ),
            ),
            parameter_t(
                name="Parameter_2",
                default=missing_required_value_t(
                    Annotated[tuple, sequence_t(lengths=(2, 3, 4))]
                ),
            ),
            parameter_t(
                name="Parameter_3",
                default=missing_required_value_t(
                    Annotated[str, choices_t(("One", "Second", "Last"))]
                ),
            ),
            parameter_t(
                name="Document",
                types=(
                    None,
                    Annotated[
                        pl_path_t, path_t(target_type=path_t.TARGET_TYPE.document)
                    ],
                ),
                default=None,
            ),
            parameter_t(
                name="Folder",
                types=Annotated[
                    pl_path_t, path_t(target_type=path_t.TARGET_TYPE.folder)
                ],
                default=pl_path_t("."),
            ),
            parameter_t(
                name="Any_Path",
                types=Annotated[pl_path_t, path_t(target_type=path_t.TARGET_TYPE.any)],
                default=pl_path_t("."),
            ),
        ],
    ),
    section_t(
        name="Section_2",
        controller=controller_t(
            section="Section_1", parameter="Parameter_3", primary_value="One"
        ),
        parameters=[
            parameter_t(
                name="Parameter_1",
                types=(
                    Annotated[int, number_t(min=10)],
                    Annotated[bool, boolean_t(mode=boolean_t.MODE.yes_no)],
                ),
                default=123,
            ),
            parameter_t(
                name="Parameter_2",
                types=Annotated[tuple, sequence_t(lengths=(2, 3, 4))],
                default=(456, 789),
            ),
        ],
        alternatives={
            "Second": [
                parameter_t(
                    name="Parameter_3",
                    types=Annotated[
                        str, choices_t(("Letter A", "Letter B", "Letter C"))
                    ],
                    default="Letter B",
                ),
                parameter_t(
                    name="Parameter_4",
                    types=(
                        None,
                        Annotated[
                            pl_path_t, path_t(target_type=path_t.TARGET_TYPE.document)
                        ],
                    ),
                    default=None,
                ),
            ],
            "Last": [
                parameter_t(
                    name="Parameter_5",
                    types=Annotated[
                        pl_path_t, path_t(target_type=path_t.TARGET_TYPE.folder)
                    ],
                    default=pl_path_t("."),
                ),
                parameter_t(
                    name="Parameter_6",
                    types=Annotated[
                        pl_path_t, path_t(target_type=path_t.TARGET_TYPE.any)
                    ],
                    default=pl_path_t("."),
                ),
            ],
        },
    ),
)

config = config_t(_SECTIONS)
