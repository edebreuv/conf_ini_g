# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2021)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import sys as sstm
import time
from pprint import pprint

import colorama as clrm

import conf_ini_g.activated.config as actv
import conf_ini_g.interface.storage.config as strg
from conf_ini_g.interface.console.config import CommandLineParser, ParsedArguments
from conf_ini_g.interface.screen.config import config_t as visual_config_t
from conf_ini_g.interface.screen.library.pyqt5_generic import widget_event_loop_t
from conf_ini_g.raw.config import AsStr, raw_config_h
from conf_ini_g.test.specification_1 import config


TITLE = "Confg 1"
INI_DOCUMENT = "config_1.ini"
cmd_line_arguments = [
    "--advanced-mode",
    "--Section_1-Folder=/bin",
    "--UNITS-light_years=1.2345",
    INI_DOCUMENT,
]


parser = CommandLineParser(TITLE, config)
parser_help = parser.format_help()
print(f"{clrm.Fore.GREEN}:: Help From Specification{clrm.Fore.RESET}")
print(parser_help)

ini_document, advanced_mode, arguments = ParsedArguments(
    parser, arguments=cmd_line_arguments
)
print(f"{clrm.Fore.GREEN}:: Command Line Arguments{clrm.Fore.RESET}")
pprint(arguments)

draft = strg.DraftSpecificationFromINIDocument(ini_document)
if draft is None:
    print("!!!!\nInvalid INI document\n!!!!")
    sstm.exit(-1)
print(f"{clrm.Fore.GREEN}:: Draft Specification from INI{clrm.Fore.RESET}")
print(draft)

config_ini = strg.INIConfigFromINIDocument(ini_document)
if config_ini is None:
    print("!!!!\nInvalid INI document\n!!!!")
    sstm.exit(-1)
print(f"{clrm.Fore.GREEN}:: INI Config{clrm.Fore.RESET}")
print(AsStr(config_ini))

issues, for_deferred_check = actv.ActivateFromINIConfig(
    config, config_ini, arguments=arguments
)
print(f"{clrm.Fore.GREEN}:: Activated Config{clrm.Fore.RESET}")
print(config)
print(config.has_default_value)
print(for_deferred_check)
if issues is not None:
    print("!!!!")
    print("\n".join(issues))
    print("!!!!")
    sstm.exit(-1)

config_raw, issues = actv.RawConfigWithConsumedUnits(config)
if issues is not None:
    print("!!!!")
    print("\n".join(issues))
    print("!!!!")
    sstm.exit(-1)
print(f"{clrm.Fore.GREEN}:: Raw Config with Consumed Units{clrm.Fore.RESET}")
pprint(config_raw)


def DisplayRawConfig(_cfg: raw_config_h, /) -> None:
    """"""
    print("----\n" + AsStr(_cfg) + "\n----\n")
    time.sleep(2)


event_loop = widget_event_loop_t(sstm.argv)
config_scr = visual_config_t.NewFromConfig(
    TITLE,
    config,
    advanced_mode=advanced_mode,
    ini_document=ini_document,
    action=("Display Raw Config", DisplayRawConfig),
)
config_scr.Show()
end_status = event_loop.Run()
sstm.exit(end_status)
