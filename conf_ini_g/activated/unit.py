# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2021)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from typing import Dict, Sequence

from conf_ini_g.specification.constant import UNIT_SECTION
from conf_ini_g.specification.parameter import parameter_t
from conf_ini_g.specification.section import section_t
from conf_ini_g.specification.unit import STD_UNIT_CONVERSIONS


def AddUnitsToSection(section: section_t, units: Dict[str, str], /) -> None:
    """"""
    for unit, value in units.items():
        unit_spec = parameter_t(
            name=unit,
            definition="",
            description="",
            basic=True,
            types=(int, float),
            default=1,
        )
        section.AddParameter(unit_spec)


def Issues(units: section_t, /) -> Sequence[str]:
    """"""
    output = []

    standard_units = tuple(STD_UNIT_CONVERSIONS.keys())
    for unit in units:
        if unit.name in standard_units:
            if unit.actual.value == STD_UNIT_CONVERSIONS[unit.name][-1]:
                message = "Unit already available among standard units; Please remove unit from INI"
            else:
                message = (
                    "Redefinition of a standard unit; Please use another unit name"
                )
            output.append(f"{UNIT_SECTION}/{unit.name}: {message}")

        output.extend(unit.Issues(section=UNIT_SECTION))

    return output
