# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2021)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import dataclasses as dtcl
from typing import Any, Dict, List

import colorama as clrm

from conf_ini_g.specification.constant import VALID_CHARACTERS
from conf_ini_g.standard.dtcl_extension import AsStr


@dtcl.dataclass(repr=False, eq=False)
class generic_t:
    name: str
    definition: str = "No Definition Provided"
    description: str = "No Description Provided"
    basic: bool = True

    def AsDict(self) -> Dict[str, Any]:
        """"""
        return dtcl.asdict(self)

    def Issues(self, /, *, context: str = None) -> List[str]:
        """"""
        output = []

        if context is None:
            context = ""
        else:
            context = f' in "{context}"'

        if any(_chr not in VALID_CHARACTERS for _chr in self.name):
            output.append(
                f"{self.name}: Name{context} contains invalid characters; "
                f"Valid={VALID_CHARACTERS}"
            )
        if not isinstance(self.definition, str):
            output.append(
                f'{self.definition}: Not a valid string for definition of "{self.name}"{context}'
            )
        if not isinstance(self.description, str):
            output.append(
                f'{self.description}: Not a valid string for description of "{self.name}"{context}'
            )

        return output

    def __str__(self) -> str:
        """"""
        return AsStr(self)


clrm.init()
