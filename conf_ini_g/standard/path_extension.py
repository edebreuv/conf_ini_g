# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2021)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from os import PathLike as path_like_t
from pathlib import Path as cross_platform_path_t
from typing import Optional


# Always use path_t to represent pathlib paths. This guarantees that path_t(some_string) is an instance of path_t, as
# opposed to pathlib.Path(some_string) which is of type PosixPath or WindowsPath instead of Path itself.
# TODO: ok, but it is a problem of getting PosixPath or WindowsPath? isinstance(, pathlib.Path) should still work, no?
path_t = type(cross_platform_path_t("."))
any_path_h = str| bytes| path_t| path_like_t


def PathFromComponents(*components) -> path_t:
    """"""
    output = path_t(components[0])

    for component in components[1:]:
        output /= component

    return output


def AsAbsolutePath(path: str, /) -> Optional[path_t]:
    """"""
    try:
        output = path_t(path).resolve()
    except RuntimeError:
        output = None

    return output


def ValidateInputPath(
    path: path_t, /, *, should_raise_on_error: bool = False
) -> Optional[str]:
    """"""
    if not path.exists():
        error = f"{path}: Non-existing path"
        if should_raise_on_error:
            raise FileNotFoundError(error)
        else:
            return error

    if path.is_dir():
        error = f"{path}: Path is a folder; Expected=Path to an existing file"
        if should_raise_on_error:
            raise IsADirectoryError(error)
        else:
            return error

    return None


def ValidateOutputPath(
    path: path_t,
    /,
    *,
    should_overwrite: bool = False,
    should_raise_on_error: bool = False,
) -> Optional[str]:
    """"""
    if path.exists() and not should_overwrite:
        error = f"{path}: Path exists and overwriting is not allowed"
        if should_raise_on_error:
            raise PermissionError(error)
        else:
            return error

    if path.is_dir():
        if should_overwrite:
            document_qualification = ""
            document_property = ", existing or not"
        else:
            document_qualification = "non-existing "
            document_property = ""
        error = (
            f"{path}: Path is a folder; "
            f"Expected=Path to a {document_qualification}file{document_property}"
        )
        if should_raise_on_error:
            raise IsADirectoryError(error)
        else:
            return error

    return None
